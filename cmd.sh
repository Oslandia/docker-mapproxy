#!/bin/bash


ARGS=""

# prepare uWSGI arguments
[[ -n ${PROCESSES} ]] && ARGS="$ARGS --processes ${PROCESSES}"
[[ -n ${MERCY} ]] && ARGS="$ARGS --worker-reload-mercy ${MERCY}"
[[ -n ${UID} && ${UID} -ne 0 ]] && ARGS="$ARGS --uid ${UID}"
[[ -n ${GID} ]] && ARGS="$ARGS --gid ${GID}"
[[ -n ${CONN} ]] && ARGS="$ARGS --listen ${CONN}"
[[ -n ${THREADED} ]] && ARGS="$ARGS --enable-threads"

# create a MapProxy test configuration if none is provided
[[ -f /mapproxy/mapproxy.yaml ]] || mapproxy-util create -t base-config /mapproxy

# execute MapProxy
exec uwsgi --yaml /uwsgi.yml $ARGS
